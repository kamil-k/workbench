package com.table;

import org.apache.commons.cli.*;

import java.util.ArrayList;
import java.util.List;

public class CommandService {

    private CommandLineParser parser = new DefaultParser();
    private CommandLine command;

    public CommandService(){



    }

    public CommandLine parse(String[] args){
        Options options = initOptions();
        parser = new DefaultParser();
        CommandLine line = null;
        try {
            // parse the command line arguments
            line = parser.parse( options, args );
        }
        catch( ParseException exp ) {
            System.out.println( "Parsing failed.  Reason: " + exp.getMessage() );
        }
        catch( IllegalArgumentException exp ){
            System.out.println( "Validation failed.  Reason: " + exp.getMessage() );
        }
        return line;
    }

    private void validateInput(CommandLine cmd){
        if(cmd.hasOption('c')&&cmd.hasOption('d')){
            throw new IllegalArgumentException("Context data inline (d) and external source (c) are not allowed in one command");
        }
        if(cmd.hasOption('t')&&cmd.hasOption('i')){
            throw new IllegalArgumentException("Template input (i) and path (t) are not allowed in one command");
        }

    }

    private Options initOptions(){
        Options options = new Options();

        Option contextOption   = new Option("c", true,"Context data external source oath path");
        contextOption.setLongOpt("context");

        Option contextInlineOption   = new Option("d", true,"Context data inline");
        contextInlineOption.setLongOpt("context-inline");

        Option templateOption   = new Option("t", true,"Path to template");
        templateOption.setLongOpt("template");

        Option templateInlineOption   = new Option("i", true,"Handlebars inline template");
        templateInlineOption.setLongOpt("template-inline");

        Option printOption   = new Option("p", false,"Print output template process in system console");
        printOption.setLongOpt("print");

        options.addOption(printOption);
        options.addOption(contextOption);
        options.addOption(templateOption);
        options.addOption(templateInlineOption);
        options.addOption(contextInlineOption);

        return options;

    }
}
