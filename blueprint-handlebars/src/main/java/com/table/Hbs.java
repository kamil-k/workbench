package com.table;

import org.apache.commons.cli.CommandLine;

public class Hbs{
   public static void main( String[] args ){

       TemplateService templateService = new TemplateService();


       CommandService commandService = new CommandService();
       CommandLine cmd = commandService.parse(args);

       if(cmd!=null){

           Object context = null;
           String template = null;
           String output;

           if(cmd.hasOption("c")){
               context = cmd.getOptionValue("c");
           }
           if(cmd.hasOption("i")){
               template = cmd.getOptionValue("i");
           }
           output = templateService.processTemplate(context,template);
           if(cmd.hasOption("p")){
               System.out.println(output);
           }





       }
       else{
           return;
       }
   }
}