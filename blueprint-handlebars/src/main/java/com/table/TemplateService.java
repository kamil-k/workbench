package com.table;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;

import java.io.IOException;

public class TemplateService {

    private Handlebars handlebars = new Handlebars();

    public String processTemplate(Object context, String template){

        Template compiledTemplate = null;
        try {
            compiledTemplate = handlebars.compileInline("Hello {{this}}!");
            return compiledTemplate.apply("Handlebars.java");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

}
